<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nlc
 */

?>

<?php wp_footer(); ?>

<!--
<?php echo esc_url( __( 'https://wordpress.org/', 'nlc' ) ); ?>
<?php
	/* translators: %s: CMS name, i.e. WordPress. */
	printf( esc_html__( 'Proudly powered by %s', 'nlc' ), 'WordPress' );
?>
-->

</body>
</html>
