<?php
/* Template Name: Home */

include __DIR__ . '/common.php';
wp_enqueue_style('home', get_stylesheet_directory_uri() . '/home.css');

get_header();

while (have_posts()) : the_post();
?>

<section class='HomeLogo'>
    <div class='container'>
        <div class='HomeLogo-inner'>
            <img class='HomeLogo-img' src='<?php echo get_field('logo'); ?>' alt='NLC' width='562' height='327' />
        </div>
    </div>

    <?php include __DIR__ . '/more.php'; ?>
</section>

<main class='HomeContent'>
    <div class='HomeContent-container'>
        <div class='HomeContent-inner'>
            <?php the_content(); ?>
        </div>
    </div>
</main>

<section class='HomeExperts'>
    <div class='HomeExperts-container'>
        <div class='HomeExperts-inner'>
            <div class='HomeExperts-decor'></div>

            <h2 class='HomeExperts-title'>Наши эксперты</h2>

            <?php
            $query = new WP_Query(array(
                'post_type' => 'expert'
            ));

            while ($query->have_posts()) {
                $query->the_post();
            ?>

            <article class='HomeExpert'>
                <div class='HomeExpert-row'>
                    <div class='HomeExpert-info'>
                        <h3 class='HomeExpert-title'><?php the_title(); ?></h3>

                        <div><?php the_content(); ?></div>

                        <p class='HomeExpert-more'>
                            <a href='#'>Подробнее</a>
                        </p>
                    </div>

                    <div class='HomeExpert-photo'>
                        <?php the_post_thumbnail(); ?>
                    </div>
                </div>
            </article>

            <?php
            }

            wp_reset_postdata();
            ?>
        </div>
    </div>
</section>

<section class='HomePartners'>
    <div class='HomePartners-inner'>
        <div class='HomePartners-container'>
            <h2 class='HomePartners-title'>Наши партнеры</h2>

            <div class='HomePartners-logos'>
                <?php
                $query = new WP_Query(array(
                    'order' => 'ASC',
                    'orderby' => 'date',
                    'post_type' => 'partner'
                ));

                while ($query->have_posts()) : $query->the_post();
                ?>

                <a class='HomePartner' href='<?php the_field("partner_url"); ?>' title='<?php the_title(); ?>'>
                    <?php echo get_the_post_thumbnail(); ?>
                </a>
                <?php
                endwhile; wp_reset_postdata();
                ?>
            </div>
        </div>

        <?php include __DIR__ . '/nlc-footer.php'; ?>
    </div>
</section>

<?php
endwhile;

get_footer();