<?php
/* Template Name: Events */

include __DIR__ . '/common.php';
wp_enqueue_style('events', get_stylesheet_directory_uri() . '/events.css');

get_header();
?>

<?php include __DIR__ . '/nlc-header.php'; ?>

<section class='Section Events'>
    <div class='container'>
        <div class='EventCalendars'>
            <?php
            echo get_nlc_calendar(2017, 6);
            echo get_nlc_calendar(2017, 7);
            echo get_nlc_calendar(2017, 8);
            ?>
        </div>
    </div>
</section>

<section class='Section EventsPosts'>
    <div class='container'>
        <div class='EventsPosts-inner'>
            <?php
            $query = new WP_Query(array(
                'order' => 'DESC',
                'orderby' => 'date',
                'post_type' => 'event'
            ));

            while ($query->have_posts()) : $query->the_post();
            ?>
            <article class='EventsPost'>
                <?php echo get_the_post_thumbnail(); ?>

                <h2 class='EventsPost-title'><?php the_title(); ?></h2>

                <p class='EventsPost-city'><?php echo get_field('event-city'); ?></p>
            </article>

            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </div>
</section>

<?php include __DIR__ . '/nlc-footer.php'; ?>

<?php get_footer(); ?>

<?php

function get_link_by_year_month_day() {
    $linkByYearMonthDay = array();
    
    $query = new WP_Query(array(
        'order' => 'DESC',
        'orderby' => 'date',
        'post_type' => 'event'
    ));

    while ($query->have_posts()) : $query->the_post();

    $time = get_post_time('U', true);

    $year = date('Y', $time);
    $month = date('n', $time);
    $day = date('j', $time);

    $key = $year.'-'.$month.'-'.$day;

    if (!isset($linkByYearMonthDay[$key])) {
        $linkByYearMonthDay[$key] = get_permalink();
    }

    endwhile; wp_reset_postdata();

    return $linkByYearMonthDay;
}

function get_nlc_calendar($year, $month) {
    $linkByYearMonthDay = get_link_by_year_month_day();

    ob_start();
    ?>

    <div class='EventCalendar'>
        <div class='EventCalendarDay'>Пн</div>
        <div class='EventCalendarDay'>Вт</div>
        <div class='EventCalendarDay'>Ср</div>
        <div class='EventCalendarDay'>Чт</div>
        <div class='EventCalendarDay'>Пт</div>
        <div class='EventCalendarDay'>Сб</div>
        <div class='EventCalendarDay'>Вс</div>

        <?php for ($i = get_last_monday_of_month($year, $month - 1); $i <= get_last_day_of_month($year, $month - 1); $i++) : ?>
            <div class='EventCalendarDay'>
                <?php echo $i; ?>
            </div>
        <?php endfor; ?>

        <?php for ($i = 1; $i <= get_last_day_of_month($year, $month); $i++) : ?>
            <?php $key = $year.'-'.$month.'-'.$i; ?>
            <?php $hasEvents = isset($linkByYearMonthDay[$key]); ?>

            <?php if ($hasEvents) : ?>
                <a class='EventCalendarDay EventCalendarDay--hasEvents' href='<?php echo $linkByYearMonthDay[$key]; ?>'>
                    <?php echo $i; ?>
                </a>
            <?php else : ?>
                <div class='EventCalendarDay'>
                    <?php echo $i; ?>
                </div>
            <?php endif; ?>
        <?php endfor; ?>
    </div>

    <?php
    return ob_get_clean();
}

function get_last_day_of_month($year, $month) {
    $month++;
    return intval(date("t", strtotime("$year-$month-00")));
}

function get_last_monday_of_month($year, $month) {
    $last_day = get_last_day_of_month($year, $month);

    $day = $last_day;

    while (intval(date("w", strtotime("$year-$month-$day"))) !== 1) {
        $day--;
    }

    return $day;
}