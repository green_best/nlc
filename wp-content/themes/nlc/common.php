<?php

wp_enqueue_style('alexander', get_stylesheet_directory_uri() . '/alexander/alexander.css');
wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/bootstrap/css/bootstrap.min.css');
wp_enqueue_style('material-design-iconic-font', get_stylesheet_directory_uri() . '/material-design-iconic-font/css/material-design-iconic-font.min.css');

wp_enqueue_style('title', get_stylesheet_directory_uri() . '/title.css');
wp_enqueue_style('nlc-header', get_stylesheet_directory_uri() . '/nlc-header.css');
wp_enqueue_style('nlc-footer', get_stylesheet_directory_uri() . '/nlc-footer.css');

wp_enqueue_script('bootstrap', get_stylesheet_directory_uri() . '/bootstrap.native/dist/bootstrap-native-v4.min.js', array(), false, true);