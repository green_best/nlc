<header class='Header'>
    <a class='Header-item Header-logo' href='#'>
       <img src='/wp-content/uploads/2017/09/logo.svg' alt='NLC' width='30' height='30' />
    </a>

    <a class='Header-item Header-menu' href='#'>
        <i class="zmdi zmdi-menu"></i>
    </a>

    <a class='Header-item Header-search' href='#'>
        <i class="zmdi zmdi-search"></i>
    </a>
</header>
