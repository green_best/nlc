<?php
/*
Template Name: Article
Template Post Type: post
*/

include __DIR__ . '/common.php';

wp_enqueue_style('article', get_stylesheet_directory_uri() . '/article.css');

wp_enqueue_script('max-bug', get_template_directory_uri() . '/js/max-bug.js');

get_header();

include __DIR__ . '/nlc-header.php'; ?>

<main class='Post'>

	<div class='Post-wrap'>

		<?php while ( have_posts() ) : the_post(); ?>

		<p class='PostPath'>
			<a href='<?php echo home_url(); ?>'>New Line Clinic</a>
			<?php the_category('single'); ?>
			<a href='<?php echo get_permalink(); ?>'><?php the_title(); ?></a>
		</p>

		<p class='PostBack'>
			<?php $categories = get_the_category();
				$category_id = $categories[0]->cat_ID;
				$category_link = get_category_link( $category_id );
			?>
			<a href='<?php echo $category_link; ?>'>
				<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 11 10.02' class='PostBack-arrow' ><title>arrow_back</title><g id='Layer_2' data-name='Layer 2'><g id='Layer_1-2' data-name='Layer 1'><polygon points='6.27 8.77 5.01 10.03 0 5.01 5.01 0 6.27 1.25 3.39 4.13 11 4.13 11 5.9 3.39 5.9 6.27 8.77 6.27 8.77 6.27 8.77'/></g></g></svg>
				назад
			</a>
		</p>

	</div>

	<article class='Article'>

		<p class='ArticleDate'><?php echo get_the_date('d.m.y'); ?></p>
		<h1 class='ArticleTitle'><?php the_title(); ?></h1>
		<section class='ArticleContent'><?php the_content(); ?></section>
		<p class='ArticleTag'><?php echo get_the_tag_list(); ?></p>

		<section class='ArticleNav'>
			<div class='ArticleNav-prev'>
				<?php
					$prev_post = get_previous_post(true);
					if(! empty($prev_post)) :
				?>
				<a href='<?php echo get_permalink( $prev_post->ID ); ?>' title='<?php echo $prev_post->post_title; ?>'>
					<p>предыдущая</p>
					<p><?php echo $prev_post->post_title; ?></p>
				</a>
				<?php endif; ?>
			</div>
			<div class='ArticleNav-next'>
				<?php
					$next_post = get_next_post(true);
					if( ! empty($next_post) ) :
				?>
				<a href='<?php echo get_permalink( $next_post->ID ); ?>' title='<?php echo $next_post->post_title; ?>'>
					<p>следующая</p>
					<p ><?php echo $next_post->post_title; ?></p>
				</a>
				<?php endif; ?>
			</div>
		</section>

	</article>

	<?php endwhile; ?>

</main>

<?php include __DIR__ . '/nlc-footer.php';

get_footer(); ?>