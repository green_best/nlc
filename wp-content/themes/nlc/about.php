<?php
/* Template Name: About */

include __DIR__ . '/common.php';
wp_enqueue_style('nlcabout', get_stylesheet_directory_uri() . '/about.css');
wp_enqueue_script('googlemaps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDX2R899Efc4BPHSOieBOSUg9ibnAqvQv8&callback=initMap', array(), false, true);

get_header();
?>
<?php while (have_posts()) : the_post(); ?>

<section class='container py-4 About'>
    <h1 class='Title'><?php the_title(); ?></h1>

    <div class='row About-row'>
        <div class='col-12 col-lg-5 About-content'><?php the_content(); ?></div>

        <div class='col-12 col-lg-7 About-aside'>
            <div class='About-map'>
                <button class='About-mask' type='button'>Кликните, чтобы масштабировать.</button>

                <div class='About-google'></div>
            </div>
        </div>
    </div>
</section>

<section class='AboutForm'>
    <h2 class='AboutForm-title'>Свяжитесь с нами</h2>

    <?php echo do_shortcode('[contact-form-7 id="47" title="Контакты"]'); ?>
</section>

<?php include __DIR__ . '/nlc-footer.php'; ?>

<script>
    function initMap() {
        var center = {
            lat: <?php the_field('about-latitude'); ?>,
            lng: <?php the_field('about-longitude'); ?>
        };

        var elem = document.querySelector('.About-google');

        var map = new google.maps.Map(elem, {
            zoom: 15,
            center: center
        });

        var marker = new google.maps.Marker({
            position: center,
            map: map
        });

        var infowindow = new google.maps.InfoWindow({
            content: '<?php the_field('about-infowindow'); ?>'
        });

        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

        google.maps.event.addListener(map, 'click', function () {
            infowindow.close();
        });
    }

    var mask = document.querySelector('.About-mask');

    mask.addEventListener('click', function () {
        mask.hidden = true;
    });
</script>

<?php endwhile; wp_reset_postdata(); ?>

<?php get_footer();