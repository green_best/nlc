window.onload = function () {

	var article = document.querySelector('.ArticleContent');

	var img = article.querySelectorAll('.size-full');

	for (var i = 0; i < img.length; i++) {

		img[i].parentNode.classList.add('ArticleContent-wide');
		
	}



	var nextEmpty = document.querySelector('.ArticleNav-next a');

	var prevEmpty = document.querySelector('.ArticleNav-prev a');

	var prev = document.querySelector('.ArticleNav-prev');

	window.addEventListener('resize', resize);

		function resize() {

			if (!nextEmpty && window.innerWidth <= 560) {

				prev.classList.add('ArticleNav-prevEmpty');

			}

			if (!prevEmpty && window.innerWidth <= 560) {

				prev.classList.add('ArticleNav-prevEmpty');

			}
		}
};