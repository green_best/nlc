<?php $locations = get_nav_menu_locations();?>

<footer class='Footer'>
    <div class='Footer-container'>
        <?php $query = new WP_Query(array('page_id' => 8)); ?>

        <?php while ($query->have_posts()) : $query->the_post(); ?>
            <section class='FooterContacts'>
                <div class='FooterTitle'><?php the_title(); ?></div>

                <div class='FooterContacts-content'><?php the_content(); ?></div>
            </section>

            <?php wp_reset_postdata(); ?>
        <?php endwhile; ?>

        <nav class='FooterAbout'>
            <?php
                $menuId = $locations['about'];
                $menu = wp_get_nav_menu_object($menuId);
            ?>

            <div class='FooterTitle'><?php echo $menu->name; ?></div>

            <?php wp_nav_menu(array('theme_location' => 'about')); ?>
        </nav>

        <nav class='FooterBranches'>
            <?php
                $menuId = $locations['branches'];
                $menu = wp_get_nav_menu_object($menuId);
            ?>

            <div class='FooterTitle'><?php echo $menu->name; ?></div>

            <?php wp_nav_menu(array('theme_location' => 'branches')); ?>
        </nav>

        <section class='FooterFeedback'>
            <button type='button' class='btn FooterFeedback-btn' data-toggle='modal' data-target='#feedback'>Напишите нам</button>
        </section>
    </div>
</footer>

<div class='modal fade' id='feedback' role='dialog'>
    <div class='modal-dialog' role='document'>
        <div class='modal-content'>
            <div class='modal-body'>
                <?php echo do_shortcode('[contact-form-7 id="4" title="Напишите нам"]'); ?>
            </div>
        </div>
    </div>
</div>